fm-timepicker
=============

[Demo site](http://hartwig-at.github.io/fm-timepicker/demo.html)

A simple time picker for **AngularJS** based on **Bootstrap** and **Moment**.

![](http://i.imgur.com/CvrVZXX.png) 
